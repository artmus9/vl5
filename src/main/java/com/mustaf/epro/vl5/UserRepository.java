package com.mustaf.epro.vl5;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    List<User> findUsersByEmail(String email);

    /*@Query(
            value = "select * from User user where user.email = :email",
            nativeQuery = true)*/
    @Query("select user from User user where user.email = :email")
    User findUserByEmail(@Param("email") String email);

    /*@Query(
            value = "select * from User user where user.email <> user.password",
            nativeQuery = true)*/
    @Query("select user from User user where user.email = user.password")
    List<User> findUsersByEmailSameAsPassword();
}
