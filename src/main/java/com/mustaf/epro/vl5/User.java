package com.mustaf.epro.vl5;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "\"user\"")
public class User extends AbstractEntity {
    public String fistName;
    public String name;
    public String email;
    public String password;

    @OneToMany
    private Set<Group> groups;
}
