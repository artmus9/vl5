package com.mustaf.epro.vl5;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "\"group\"")
public class Group extends AbstractEntity {
    private String name;
    private String description;

    @ManyToMany
    @JoinTable(
            name = "group_right",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "right_id")}
    )
    private Set<Right> rights = new HashSet<>();
}
