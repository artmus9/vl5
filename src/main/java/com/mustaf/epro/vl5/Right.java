package com.mustaf.epro.vl5;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "\"right\"")
public class Right extends AbstractEntity {
    private String name;
    private String description;
    private String key;

    @ManyToMany(mappedBy = "rights")
    private Set<Group> groups = new HashSet<>();
}
