package com.mustaf.epro.vl5;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.output.OutputFrame;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@Transactional
/*@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
 */
class UserRepositoryContainerIT {

    @Container
    private static final PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:latest");

    @Container
    private static final MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:latest"));

    @DynamicPropertySource
    static void overrideApplicationProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
    }

    @Autowired
    UserRepository userRepository;

    @Test
    public void should_return_user_by_existing_email() {
        User user = new User();
        user.fistName = "Max";
        user.name = "Mustermann";
        user.email = "something@mail.com";
        user.password = "123456";

        User user1 = new User();
        user1.fistName = "Lucia";
        user1.name = "Schwanhild";
        user1.email = "schwanhild@mail.com";
        user1.password = "123456789";

        User user2 = new User();
        user2.fistName = "Abraham";
        user2.name = "Philomena";
        user2.email = "philomena@mail.com";
        user2.password = "qwerty";

        User user3 = new User();
        user3.fistName = "Carina";
        user3.name = "Sibylla";
        user3.email = "sibylla@mail.com";
        user3.password = "sibylla@mail.com";

        userRepository.save(user);
        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);

        assertThat(userRepository.findUserByEmail("philomena@mail.com"))
                .isEqualTo(user2);
    }

    @Test
    public void should_return_null_by_nonexistent_email() {
        User user = new User();
        user.fistName = "Max";
        user.name = "Mustermann";
        user.email = "something@mail.com";
        user.password = "123456";

        User user1 = new User();
        user1.fistName = "Lucia";
        user1.name = "Schwanhild";
        user1.email = "schwanhild@mail.com";
        user1.password = "123456789";

        User user2 = new User();
        user2.fistName = "Abraham";
        user2.name = "Philomena";
        user2.email = "philomena@mail.com";
        user2.password = "qwerty";

        User user3 = new User();
        user3.fistName = "Carina";
        user3.name = "Sibylla";
        user3.email = "sibylla@mail.com";
        user3.password = "sibylla@mail.com";

        userRepository.save(user);
        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);

        assertThat(userRepository.findUserByEmail("1234@5678.com")).isNull();
    }

    @Test
    public void should_return_user_by_email_same_as_password() {
        User user = new User();
        user.fistName = "Max";
        user.name = "Mustermann";
        user.email = "something@mail.com";
        user.password = "123456";

        User user1 = new User();
        user1.fistName = "Lucia";
        user1.name = "Schwanhild";
        user1.email = "schwanhild@mail.com";
        user1.password = "123456789";

        User user2 = new User();
        user2.fistName = "Abraham";
        user2.name = "Philomena";
        user2.email = "philomena@mail.com";
        user2.password = "qwerty";

        User user3 = new User();
        user3.fistName = "Carina";
        user3.name = "Sibylla";
        user3.email = "sibylla@mail.com";
        user3.password = "sibylla@mail.com";

        userRepository.save(user);
        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);

        assertThat(userRepository.findUsersByEmailSameAsPassword())
                .containsOnly(user3);
    }
}